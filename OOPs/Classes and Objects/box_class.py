
class Box:
    def __init__(self, side):
        self.side = side

    def setDimension(self):
        print ("Side:", self.side)

    def volume(self):
        vol = self.side * self.side * self.side
        print ("The Volume is:", vol)


b1 = Box(int(input("Enter the side: ")))
b1.setDimension()
b1.volume()
