# Classes and Objects

class MyCircle:
    
    def __init__(self, r):
        self.radius = r

    def areaOfCircle(self):
        area = 3.14*self.radius*self.radius
        print ("Area of circle is =", area )
    def circumOfCircle(self):
        circum = 2*3.14*self.radius
        print ("Circumference of circle is =", circum)

obj1 = MyCircle(int(input("Enter the radius:- ")))
obj1.areaOfCircle()
obj1.circumOfCircle()
obj2 = MyCircle(int(input("Enter the radius:- ")))
obj2.areaOfCircle()
obj2.circumOfCircle()
#print (obj1.radius)
#print (obj2.radius)
