# Design a class Rectangle with Data members: 1. length 2. breadth

class Rectangle:
    def __init__(self, length, breadth):
        self.length = length
        self.breadth = breadth

    def setDimension(self):
        print ("This is the length:-",self.length)
        print ("This is the breadth",self.breadth)

    def area(self):
        ar = self.length*self.breadth
        print ("The area is", ar)

    def perimeter(self):
        pm = 2*(self.length + self.breadth)
        print ("The Perimeter is", pm)

x= int(input("Enter the length: "))
y = int(input("Enter the breadth: "))

r1 = Rectangle(x, y)
r1.setDimension()
r1.area()
r1.perimeter()
