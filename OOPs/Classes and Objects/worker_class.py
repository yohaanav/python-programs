# Design a class Worker. Data members: wages, weekdays

class Worker:
    def __init__(self, wages, monthly):
        self.wages = wages
        self.month = monthly

    def setData(self):
        print ("These are your wages:", self.wages)
        print ("These are your monthly:", self.month)

    def payment(self):
        pay = self.wages*self.month
        print ("This is your salary:", pay)

w1 = Worker(1000, 5)
w1.setData()
w1.payment()
