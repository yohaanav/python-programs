# Inheritance

class Animal:

    def eat(self):
        print ("Animals eat")

    def sleep(self):
        print ("Animals sleep")

class SocialAnimal(Animal):
    def share(self):
        print ("Social Animals share")
    def speak(self):
        print ("Social Animals speak")

class Dog(Animal):
    def bark(this):
        print ("Dogs bark")

aobj = Animal();
aobj.eat();
aobj.sleep();

sobj = SocialAnimal()
sobj.eat()
sobj.sleep()
sobj.share()
sobj.speak()

dobj = Dog();
dobj.sleep();
dobj.bark();



    
