# WAP to read 3 digit number and check if the sum of the cube of its digits are equal to the number or not

num = int(input("Enter a 3-digit number: "))

fd= int(num/100)%10
sd= int(num/10)%10
ld= num%10

cfd= fd*fd*fd
csd= sd*sd*sd
cld= ld*ld*ld

sum= int(cfd+csd+cld)
print ("The sum is:",sum)

if sum==num:
    print("The sum of the cube of its digits are equal to the number!")
else:
    print("The sum of the cube of its digits are not equal to the number.")


