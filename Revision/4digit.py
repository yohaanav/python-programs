# WAP to read a 4 digit number and find the sum of its digits

num = int(input("Enter a 4 digit number: "))

fd = int(num/1000)
sd = int(num/100)%10
td = int(num/10)%10
ld = num%10

print ("First digit: ",fd)
print ("Second digit: ",sd)
print ("Third digit: ",td)
print ("Last digit: ",ld)
print ()

sum = fd + sd + td + ld

print ("The sum is: ",sum)
print ()

reverse = ld*1000 + td*100 + sd*10 + fd*1

print ("The reverse of your number is: ")
print (reverse)
