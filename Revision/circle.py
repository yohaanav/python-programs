# WAP to read radius of circle and calculate Area and Circumference

r = int(input("Enter the radius: "))

area = 3.14 * r * r

circum = 2* 3.14 * r

print ("Therefore the area of a circle is = ", area, "and the circumference is = ", ("%.2f" % circum))
