import gspread
from oauth2client.service_account import ServiceAccountCredentials

scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
creds = ServiceAccountCredentials.from_json_keyfile_name('Sales-32112f8d895c.json', scope)
client = gspread.authorize(creds)

sheet = client.open('Sample Data for Modeling Google Spread').sheet1

sample = sheet.get_all_records()
print (sample)
