# Program to display the Fibonacci sequence up to n-th term where n is provided by the user

num = 10

# uncomment to take input from the user
#nterms = int(input("How many terms? "))

# first two terms
a = 0
b = 1

for i in range(num):
    print(a)
    c = a + b
    a = b
    b = c
    
       
