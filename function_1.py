# def is identifier that this a function.
# msg is a function name
# a is argument/parameter of that function
# first line is function head and 2nd line is function body


def msg(a):
    print("Hello {} !!".format(a))

msg("World")

msg("India")
