# WAF to return greatest of 3 numbers

num1 = int(input("Enter 1st number "))

num2 = int(input("Enter 2nd number "))

num3 = int(input("Enter 3rd number "))

def greatestOfThree(num1, num2, num3):
    if num1>=num2 and num1>=num3:
        return num1
    elif num2>=num3:
        return num2
    else:
        return num3

print (greatestOfThree(num1, num2, num3), "is greatest.")
