# WAF to return Mean of numbers which is passed in a Array.

nums = [6, 5, 8, 2, 7, 4]
l = len(nums)
print ("Array count = ", l)
def meanOfNums(nums, l):
    i = 0
    total = 0
    while i<l:
        total = total + nums [i]
        i = i+1
        mean = total/l
    return mean
print ("%.2f" % meanOfNums(nums, l))
