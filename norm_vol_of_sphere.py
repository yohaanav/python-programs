# WAF to return Volume of Sphere

def volOfSphere(rad):
    vol = 4/3*3.14*rad*rad*rad
    return vol

print ("Enter the radius for the volume of a sphere ")

rad = int(input("Enter the radius of the sphere: "))

print ("%.2f"%volOfSphere(rad))
