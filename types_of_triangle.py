# WAP to read 3 angles and check if triangle can be formed or not.
# If triangle can be formed then check if it is equilateral, isosceles or right angled triangle.

print ("Check angles of a triangle")

ang1 = int(input("Enter 1st angle:- "))

ang2 = int(input("Enter 2nd angle:- "))

ang3 = int(input("Enter 3rd angle:- "))


if (ang1+ang2+ang3)==180:
    print ("Triangle can be formed")
    if (ang1==60 and ang2==60 and ang3==60):
        print ("This is an equilateral triangle")
    elif (ang1==ang2 or ang1==ang3 or ang2==ang3):
        print("This is an Isoceles Triangle ")
    elif (ang1==90 or ang2==90 or ang3==90):
        print ("This a Right-angled triangle")
else:
    print ("Triangle cannot be formed")

    
